import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './auth/signin/signin.component';
import { NavbarComponent } from './shared/ui/navbar/navbar.component';
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { AdminlistallComponent } from './admin/adminlistall/adminlistall.component';
import { AdminprofileComponent } from './admin/adminprofile/adminprofile.component';
import { AdduserComponent } from './admin/adduser/adduser.component';
import { ExecutivehomeComponent } from './executive/executivehome/executivehome.component';
import { ExenavbarComponent } from './shared/ui/exenavbar/exenavbar.component';
import { AdminsettingsComponent } from './admin/adminsettings/adminsettings.component';
import { AdminchangepassComponent } from './admin/adminchangepass/adminchangepass.component';
import { UploadphotoComponent } from './admin/uploadphoto/uploadphoto.component';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokeninterceptorService } from './service/tokeninterceptor.service';
import { ApidataService } from './service/authservice.service';
import { AuthGuard } from './auth.guard';
import { BearerService } from './service/bearer.service';
import { ExesettingsComponent } from './executive/exesettings/exesettings.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    NavbarComponent,
    AdminhomeComponent,
    AdminlistallComponent,
    AdminprofileComponent,
    AdduserComponent,
    ExecutivehomeComponent,
    ExenavbarComponent,
    AdminsettingsComponent,
    AdminchangepassComponent,
    UploadphotoComponent,
    ExesettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right'
    })
  ],
  providers: [ApidataService, BearerService, AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokeninterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
