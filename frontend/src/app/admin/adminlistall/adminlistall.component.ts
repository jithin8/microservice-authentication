import { Component, OnInit } from '@angular/core';
import { BearerService } from 'src/app/service/bearer.service';
import { AccountModel } from 'src/app/model/AccountModel';

@Component({
  selector: 'app-adminlistall',
  templateUrl: './adminlistall.component.html',
  styleUrls: ['./adminlistall.component.css']
})
export class AdminlistallComponent implements OnInit {

  employeeList: AccountModel[];
  constructor(private bearerService: BearerService) { }

  ngOnInit() {
    this.getEmployeeList();
  }

  getEmployeeList() {
    this.bearerService.getEmployeeList().subscribe((data) => {
      this.employeeList = JSON.parse(JSON.stringify(data)).data
    }, (err) => {
      console.log("Error");
      console.log(err);
    });
  }

  changeStatus(pid) {
    this.bearerService.updateStatus(pid).subscribe((data) => {
      console.log(data);
      this.getEmployeeList();
    });

  }

}
