import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BearerService } from 'src/app/service/bearer.service';

@Component({
  selector: 'app-adminchangepass',
  templateUrl: './adminchangepass.component.html',
  styleUrls: ['./adminchangepass.component.css']
})
export class AdminchangepassComponent implements OnInit {

  myForm: FormGroup;

  constructor(private fb: FormBuilder,private service: BearerService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      password:['',Validators.required],
      newpassword:['',Validators.required]
    })
  }
  changepass(passwords){
    passwords.account_pid=localStorage.getItem('pid');
    this.service.changepass(passwords).subscribe((data)=>{
      console.log(data);      
    })    
  }

}
