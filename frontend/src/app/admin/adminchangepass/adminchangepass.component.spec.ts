import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminchangepassComponent } from './adminchangepass.component';

describe('AdminchangepassComponent', () => {
  let component: AdminchangepassComponent;
  let fixture: ComponentFixture<AdminchangepassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminchangepassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminchangepassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
