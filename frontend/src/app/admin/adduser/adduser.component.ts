import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BearerService } from 'src/app/service/bearer.service';
import { ToastrService } from 'ngx-toastr';
import { format } from 'util';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  myForm:FormGroup

  constructor(private fb:FormBuilder, private service:BearerService,private toastr: ToastrService) { }

  ngOnInit() {
    this.myForm =  this.fb.group({
      username:['',[Validators.required]],
      password:['',[Validators.required]],
      email:['',[Validators.required]]
    })
  }

  adduser(newuser){
    newuser.usertype='EXEC'
    this.service.addUser(newuser).subscribe((data)=>{
      console.log(data);    
      this.showSuccess();  
      this.myForm.reset();
    },(error)=>{

    }
    )
    
  }
  showSuccess() {
    this.toastr.success('New User Added', 'Success');
  }

  showError(){
    this.toastr.error('could not add user','Error');
  }
}
