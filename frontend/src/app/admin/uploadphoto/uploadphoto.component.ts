import { Component, OnInit } from '@angular/core';
import { BearerService } from 'src/app/service/bearer.service';

@Component({
  selector: 'app-uploadphoto',
  templateUrl: './uploadphoto.component.html',
  styleUrls: ['./uploadphoto.component.css']
})
export class UploadphotoComponent implements OnInit {
  selectedFile:File=null;
  pid:string= localStorage.getItem('proid');

  constructor(private service: BearerService) { }

  ngOnInit() {
  }

  onFileChanged(event){
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
    
  }

  upload(){
    const fd = new FormData();  
    fd.append('profileImage', this.selectedFile,this.selectedFile.name);
    fd.append('profile_pid', this.pid);
    console.log(fd);
    
    this.service.uploadimage(fd).subscribe((data)=>{
      console.log(data);
    })    
  }
}
