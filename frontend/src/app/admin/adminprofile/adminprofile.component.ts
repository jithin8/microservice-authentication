import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BearerService } from 'src/app/service/bearer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminprofile',
  templateUrl: './adminprofile.component.html',
  styleUrls: ['./adminprofile.component.css']
})
export class AdminprofileComponent implements OnInit {
  edit = false;
  myForm: FormGroup;
  pid = localStorage.getItem('pid');
  details: any;

  username: string = localStorage.getItem('username');

  constructor(private fb: FormBuilder, private service: BearerService, private route: Router) { }

  ngOnInit() {    
    this.myForm = this.fb.group({
      mobile: ['', [Validators.required]]
    })

    this.service.getprofile(this.pid).subscribe((data)=>{
      this.details = data;     
      localStorage.setItem('proid',this.details.data.pid)       
    })

  }

  onEdit() {
    this.edit = !this.edit;
  }

  updateprofile(details) {
    details.account_pid = this.pid;
    this.service.updateprofile(details).subscribe((data) => {
      // console.log(data);
    })
    this.edit = false;

  }

}
