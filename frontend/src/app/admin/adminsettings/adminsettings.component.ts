import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminsettings',
  templateUrl: './adminsettings.component.html',
  styleUrls: ['./adminsettings.component.css']
})
export class AdminsettingsComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  changepass(){
    
    this.router.navigate(['/admin_home/adminSettings/adminchangepass']);
  }

  Uploadimage(){
    this.router.navigate(['/admin_home/adminSettings/adminuploadimage'])
  }

}
