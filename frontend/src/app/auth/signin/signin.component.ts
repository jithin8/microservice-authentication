import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApidataService } from 'src/app/service/authservice.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  errorMsg;
  constructor(private router: Router, private _dataservice: ApidataService, private fb: FormBuilder) { }

  loginForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]]
  })

  onSubmit() {
    this._dataservice.loginApi(this.loginForm.value)
      .subscribe(
        res => {
          let jsonObj = JSON.parse(JSON.stringify(res))
          let data = jsonObj.data;
          if (data != null) {
            localStorage.setItem('token', data.token);
            localStorage.setItem('username', data.username);
            localStorage.setItem('email', data.email);
            localStorage.setItem('pid',data.pid);
            if(data.role_name=='ADMIN'){
              this.router.navigate(['admin_home']);
            }else{
              this.router.navigate(['exe_home']);
            }
            
          }
        },
        err => {
          this.errorMsg = err
        }
      )
  }

  ngOnInit() {
  }
}
