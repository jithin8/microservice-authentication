import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApidataService } from './service/authservice.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private _authService: ApidataService, private _router: Router) {

  }

  canActivate(): boolean {
    console.log("canactivate called");
    if (this._authService.loggedIn()) {
      console.log("canactivate called loggedin");
      return true
    } else {
      console.log("canactivate called logged out");
      this._router.navigate(['/login'])
      return false
    }
  }

}
