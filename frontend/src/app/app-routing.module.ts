import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './auth/signin/signin.component';
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { AdminlistallComponent } from './admin/adminlistall/adminlistall.component';
import { AdminprofileComponent } from './admin/adminprofile/adminprofile.component';
import { AdduserComponent } from './admin/adduser/adduser.component';
import { ExecutivehomeComponent } from './executive/executivehome/executivehome.component';
import { AdminsettingsComponent } from './admin/adminsettings/adminsettings.component';
import { AdminchangepassComponent } from './admin/adminchangepass/adminchangepass.component';
import { UploadphotoComponent } from './admin/uploadphoto/uploadphoto.component';
import { AuthGuard } from './auth.guard';
import { ExesettingsComponent } from './executive/exesettings/exesettings.component';

const routes: Routes = [
  { path: '', component: SigninComponent },
  // { path: '', component: AdminhomeComponent, canActivate: [AuthGuard] },
  // { path: 'listemployee', component: AdminlistallComponent, canActivate: [AuthGuard] },
  {
    path: 'admin_home', component: AdminhomeComponent,
    children: [
      { path: '', component: AdminprofileComponent },
      { path: 'listemployee', component: AdminlistallComponent },
      { path: 'add-user', component: AdduserComponent },
      {
        path: 'adminSettings', component: AdminsettingsComponent,
        children: [
          { path: 'adminchangepass', component: AdminchangepassComponent },
          { path: 'adminuploadimage', component: UploadphotoComponent },
          {
            path: '',
            redirectTo: '/admin_home/adminSettings/adminchangepass',
            pathMatch: 'full'
          }
        ]
      }
    ]
  },
  { path: 'exe_home', component: ExecutivehomeComponent,
  children:[
    { path: '', component: AdminprofileComponent },
    {
      path: 'exeSettings', component:ExesettingsComponent,
      children: [
        { path: 'adminchangepass', component: AdminchangepassComponent },
        { path: 'adminuploadimage', component: UploadphotoComponent },
        {
          path: '',
          redirectTo: '/exe_home/exeSettings/adminchangepass',
          pathMatch: 'full'
        }
      ]
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
