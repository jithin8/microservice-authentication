import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exesettings',
  templateUrl: './exesettings.component.html',
  styleUrls: ['./exesettings.component.css']
})
export class ExesettingsComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  changepass(){
    
    this.router.navigate(['/exe_home/exeSettings/adminchangepass']);
  }

  Uploadimage(){
    this.router.navigate(['/exe_home/exeSettings/adminuploadimage'])
  }

}
