import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExesettingsComponent } from './exesettings.component';

describe('ExesettingsComponent', () => {
  let component: ExesettingsComponent;
  let fixture: ComponentFixture<ExesettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExesettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExesettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
