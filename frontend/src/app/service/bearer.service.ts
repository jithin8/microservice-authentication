import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UserModel } from '../model/UserModel';

@Injectable({
  providedIn: 'root'
})
export class BearerService {
  base_url: string = "http://localhost:8080/";
  _list_user: string = this.base_url + "account/listemployees"
  _add_user:string = this.base_url+"account/create"
  _upload_image:string = this.base_url+"profile/uploadimage"
  _update_profile:string = this.base_url+"profile/add"
  _change_password:string= this.base_url+"account/changepassword"
  _update_user: string = this.base_url + "account/update/"
  _get_profile:string = this.base_url+"account/profile/"

  constructor(private http: HttpClient, private router: Router) { }

//get profile details
  getprofile(id){
    return this.http.get(this._get_profile+id);
  }

  //updateprofile
  updateprofile(details){
    return this.http.post(this._update_profile,details);
  }

  //uploadimage
  uploadimage(photo:FormData){
    return this.http.post(this._upload_image,photo);
  }

  //change password
  changepass(newPassword){
    return this.http.post(this._change_password,newPassword);
  }
  //add new user
  addUser(user:UserModel){
    return this.http.post(this._add_user,user);
  }
  // Api Request
  getEmployeeList() {
    return this.http.get(this._list_user).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error || "Server Error")
  }

  updateStatus(pid: string) {
    return this.http.get(this._update_user + pid).pipe(catchError(this.errorHandler))
  }


  getToken() {
    return localStorage.getItem('token')
  }
}
