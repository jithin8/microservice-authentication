import { Injectable, Injector } from '@angular/core';
import { ApidataService } from './authservice.service';
import { BearerService } from './bearer.service';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TokeninterceptorService implements HttpInterceptor {

  constructor(private bearerService: BearerService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log("interceptor");

    // add skip header to skip adding header
    if (req.headers.get("skip")) {
      return next.handle(req);
    }

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.bearerService.getToken()}`
    });
    const clone = req.clone({
      headers: headers
    })
    return next.handle(clone).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error.message || "Server Error")
  }
}
