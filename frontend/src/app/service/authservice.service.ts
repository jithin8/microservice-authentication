import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { AccountModel } from '../model/AccountModel';

@Injectable({
  providedIn: 'root'
})
export class ApidataService {
  base_url: string = "http://localhost:8080/";
  _login_url: string = this.base_url + "account/login"

  constructor(private http: HttpClient, private router: Router) { }

  // Api Request

  loginApi(account): Observable<AccountModel> {
    let headers = new HttpHeaders({
      'skip': 'true'
    });
    let options = { headers: headers };
    return this.http.post<AccountModel>(this._login_url, account, options).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error.message || "Server Error")
  }

  loggedIn() {
    console.log("jtihn")
    console.log(localStorage.getItem('token'))
    let tokenvalue = localStorage.getItem('token');
    if (tokenvalue != null && tokenvalue.length > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  getToken() {
    return localStorage.getItem('token')
  }

  logOutUser() {
    localStorage.removeItem('token')
    localStorage.removeItem('username')
    localStorage.removeItem('email')
    localStorage.removeItem('pid')
    localStorage.removeItem('proid')
    this.router.navigate([''])
  }
}