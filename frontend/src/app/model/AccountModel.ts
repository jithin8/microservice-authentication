export interface AccountModel {
    pid: string,
    username: string,
    email: string[],
    password: string,
    role: string,
    active: boolean,
}