export interface UserModel{
    username: string,
    email: string,
    password: string,
    usertype: string,
}