import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExenavbarComponent } from './exenavbar.component';

describe('ExenavbarComponent', () => {
  let component: ExenavbarComponent;
  let fixture: ComponentFixture<ExenavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExenavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExenavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
