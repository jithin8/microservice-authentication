import { Component, OnInit } from '@angular/core';
import { ApidataService } from 'src/app/service/authservice.service';

@Component({
  selector: 'app-exenavbar',
  templateUrl: './exenavbar.component.html',
  styleUrls: ['./exenavbar.component.css']
})
export class ExenavbarComponent implements OnInit {

  username: string = localStorage.getItem('username');
  constructor(public _dataservice: ApidataService) { }

  ngOnInit() {
  }

}
