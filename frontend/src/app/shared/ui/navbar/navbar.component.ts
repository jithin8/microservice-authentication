import { Component, OnInit } from '@angular/core';
import { ApidataService } from 'src/app/service/authservice.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username: string = localStorage.getItem('username');
  constructor(public _dataservice: ApidataService) { }

  ngOnInit() {
  }

}
