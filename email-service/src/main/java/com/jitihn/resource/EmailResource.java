package com.jitihn.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.jitihn.model.Account;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.ReactiveMailer;

@Path("/mail")
public class EmailResource {

    Logger LOG = LoggerFactory.getLogger(EmailResource.class);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hai() {
        return "send home";
    }
    @GET
    @Path("/{email}/{msg}")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@PathParam("email") String email, @PathParam("msg") String msg) {
        reactiveMailer.send(Mail.withText(email, "Subject : " + email, "Body " + msg))
                .thenApply(x -> Response.accepted().build());
        return "send";
    }

    @Inject
    ReactiveMailer reactiveMailer;

    @Incoming("emailtopic")
    public void consume(String message) {
        message = message.substring(1, message.length() - 1).replace("\\", "");
        Gson gson = new Gson();
        Account account = null;
        try {
            account = gson.fromJson(message, Account.class);
        } catch (Exception e) {
            LOG.info("Gson cannot parse exception");
        }

        System.out.println("get message " + message);
        LOG.info("get message " + message);
        LOG.info("sending mail ....");
        System.out.println("parsed get message " + account);

        if (account != null) {
            String email = account.getEmail();
            String password = account.getPassword();
            reactiveMailer.send(Mail.withText(email, "Email: " + email, "Password " + password))
                    .thenApply(x -> Response.accepted().build());
            LOG.info("Mail send successfully!!!");
        }
    }
}