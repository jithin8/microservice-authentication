package com.jitihn.model;

public class Account {
    public Long id;
    private String username;
    private String email;
    private Role role;
    private String password;
    private String pid;
    private Boolean active = true;

    public Account(Long id, String username, String email, Role role, String password, String pid, Boolean active) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.role = role;
        this.password = password;
        this.pid = pid;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Account [active=" + active + ", email=" + email + ", id=" + id + ", password=" + password + ", pid="
                + pid + ", role=" + role + ", username=" + username + "]";
    }

}