package com.jitihn.model;

public class Role {
    public Integer id;
    private String rolename;
    private String description;
    private String pid;

    public Role(Integer id, String rolename, String description, String pid) {
        this.id = id;
        this.rolename = rolename;
        this.description = description;
        this.pid = pid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "Role [description=" + description + ", id=" + id + ", pid=" + pid + ", rolename=" + rolename + "]";
    }
}