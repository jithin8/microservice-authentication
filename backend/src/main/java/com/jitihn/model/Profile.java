package com.jitihn.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.jitihn.dto.ProfileDTO;
import com.jitihn.utils.Utils;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Profile extends PanacheEntity {
    @OneToOne(cascade = CascadeType.ALL)
    Account account;
    String pid;
    String mobile;
    String profile_image;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public Profile(Account account) {
        this.account = account;
    }

    public Profile() {
    }

    // only while saving
    public Profile(ProfileDTO profileDTO) {
        this.account = profileDTO.getAccount();
        this.pid = this.mobile = profileDTO.getMobile();
        this.profile_image = profileDTO.getProfile_image();
        this.pid = Utils.generateUniqueId();
    }

    @Override
    public String toString() {
        return "Profile [account=" + account + ", mobile=" + mobile + ", pid=" + pid + ", profile_image="
                + profile_image + "]";
    }

}