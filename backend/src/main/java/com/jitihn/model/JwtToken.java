package com.jitihn.model;

import java.util.List;

public class JwtToken {
    private String username;
    private String pid;
    private String email;
    private List<String> groups;
    private String jwtClaims = "/META-INF/resources/JwtClaims.json";
    private String privateKey = "/META-INF/resources/privateKey.pem";
    private String publicKey = "/META-INF/resources/publicKey.pem";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public JwtToken(String username, String email, List<String> groups) {
        this.username = username;
        this.email = email;
        this.groups = groups;
    }

    public JwtToken(Account account) {
        this.username = account.getUsername();
        this.email = account.getEmail();
        this.pid = account.getPid();
    }

    public String getJwtClaims() {
        return jwtClaims;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    @Override
    public String toString() {
        return "JwtToken [email=" + email + ", groups=" + groups + ", jwtClaims=" + jwtClaims + ", privateKey="
                + privateKey + ", publicKey=" + publicKey + ", username=" + username + "]";
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}