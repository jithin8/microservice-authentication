package com.jitihn.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.jitihn.dto.RoleDTO;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Cacheable
public class Role extends PanacheEntityBase {

    @Id
    @SequenceGenerator(name = "roleSequence", sequenceName = "role_id_seq", allocationSize = 1, initialValue = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roleSequence")
    public Integer id;

    @Column(unique = true, nullable = false)
    private String rolename;
    private String description;

    private String pid;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
    private List<Account> accounts;

    public Role(String rolename, String description) {
        this.rolename = rolename;
        this.description = description;
    }

    public Role() {
    }

    public Role(RoleDTO roleDTO) {
        this.rolename = roleDTO.getRolename();
        this.description = roleDTO.getDescription();
        this.pid = roleDTO.getPid();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public static Role findByRole(String role) {
        System.out.println("find by role jithin " + role);
        return find("role", role).firstResult();
    }

    @Override
    public String toString() {
        return "Role [accounts=" + accounts + ", description=" + description + ", pid=" + pid + ", rolename=" + rolename
                + "]";
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

}