package com.jitihn.resource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderErrorException;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.dropbox.core.v2.sharing.RequestedVisibility;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dropbox.core.v2.sharing.SharedLinkSettings;

public class StaticContentResource {

    String ACCESS_TOKEN = "";
    DbxClientV2 client;

    // ref link : https://github.com/dropbox/dropbox-sdk-java

    public StaticContentResource() {
        ACCESS_TOKEN = "pPwHXnTsbIAAAAAAAAAAEu1U6rURLuKOYf1fym4s8Z1fEH4MuzuhBnjtFZLSxW6V";
        DbxRequestConfig config = DbxRequestConfig.newBuilder("microservice").build();
        client = new DbxClientV2(config, ACCESS_TOKEN);
    }

    public String uploadFile(String filename, String pathFileName)
            throws UploadErrorException, DbxException, IOException {
        // Upload "test.txt" to Dropbox
        System.out.println("file name " + filename);
        System.out.println("pathfile name" + pathFileName);
        try (InputStream in = new FileInputStream(pathFileName)) {
            FileMetadata metadata = client.files().uploadBuilder("/" + filename).uploadAndFinish(in);

            SharedLinkMetadata slm = client.sharing().createSharedLinkWithSettings("/" + filename,
                    SharedLinkSettings.newBuilder().withRequestedVisibility(RequestedVisibility.PUBLIC).build());

            String shareLink = slm.getUrl();
            String processed = shareLink.replace("www.dropbox.com", "dl.dropboxusercontent.com");
            return processed;
        }
    }

    public void listFolder() throws ListFolderErrorException, DbxException {
        ListFolderResult result = client.files().listFolder("");
        while (true) {
            for (Metadata metadata : result.getEntries()) {
                System.out.println(metadata.getPathDisplay());
            }

            if (!result.getHasMore()) {
                break;
            }

            result = client.files().listFolderContinue(result.getCursor());
        }
    }

}