package com.jitihn.resource;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dao.AccountRepository;
import com.jitihn.dao.ProfileRepository;
import com.jitihn.dto.ProfileDTO;
import com.jitihn.model.Account;
import com.jitihn.model.Profile;
import com.jitihn.parser.ProfileParser;
import com.jitihn.utils.Constant;
import com.jitihn.utils.Utils;

import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/profile")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProfileResource {

    Logger logger = LoggerFactory.getLogger(ProfileResource.class);

    @Inject
    ProfileRepository profileRepository;

    @Inject
    AccountRepository accountRepository;

    @POST
    @Transactional
    @Path("/add")
    public Response add(ProfileDTO profileDTO) {
        profileDTO.setPid(Utils.generateUniqueId());
        String profileOfAccountPid = profileDTO.getAccount_pid();
        Account a = accountRepository.findByPid(profileOfAccountPid);
        // check if there is account
        if (a != null) {
            // check if profile with accountid
            Profile p = profileRepository.findByAccountId(a.getId());
            if (p != null) {
                // update profile
                Profile profile_up = profileRepository.findByPIdForUpdate(p.getPid());
                String mob = profileDTO.getMobile();
                String pfImage = profileDTO.getProfile_image();
                if (mob != null && mob.length() > 0) {
                    profile_up.setMobile(mob);
                }
                if (pfImage != null && pfImage.length() > 0) {
                    profile_up.setProfile_image(pfImage);
                }
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("data", new ProfileParser(profile_up));
                return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_UPDATED, true, obj);
            } else {
                // add profile
                profileDTO.setAccount(a);
                Profile profile = new Profile(profileDTO);
                profile.persist();
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("data", new ProfileParser(profile));
                return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.CREATED_SUCCESSFULLY, true, obj);
            }
        } else {
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @ConfigProperty(name = "microservice.uploaddirectory")
    String directory;

    StaticContentResource staticResource = new StaticContentResource();

    @POST
    @Path("/uploadimage")
    @Transactional
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response extractTextFromPdf(MultipartFormDataInput input) throws BadRequestException {
        String profileImageDir = directory + "/profile";
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("profileImage");
        List<InputPart> profilePid = uploadForm.get("profile_pid");

        String pfPid;
        try {
            pfPid = profilePid.get(0).getBodyAsString();
            System.out.println(pfPid);
            Profile profile = profileRepository.findByPIdForUpdate(pfPid);
            if (profile != null) {
                // profile found
                System.out.println("profile not null");
                if (inputParts != null) {
                    for (InputPart inputPart : inputParts) {
                        try {
                            MultivaluedMap<String, String> header = inputPart.getHeaders();
                            String fileName = Utils.getFileName(header);
                            fileName = pfPid + "." + fileName.split("\\.")[1];
                            // convert the uploaded file to inputstream
                            InputStream inputStream = inputPart.getBody(InputStream.class, null);
                            byte[] bytes = IOUtils.toByteArray(inputStream);
                            File customDir = new File(profileImageDir);
                            if (!customDir.exists()) {
                                customDir.mkdir();
                            }
                            String pathfileName = customDir.getCanonicalPath() + File.separator + fileName;
                            Utils.writeFile(bytes, pathfileName);
                            String url = staticResource.uploadFile(fileName, pathfileName);

                            profile.setProfile_image(url);

                            HashMap<String, Object> obj = new HashMap<>();
                            obj.put("data", new ProfileParser(profile));
                            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_UPDATED, true, obj);
                        } catch (Exception e) {
                            e.printStackTrace();
                            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.SOMETHING_WENT_WRONG,
                                    false, null);
                        }
                    }
                } else {
                    System.out.println("inputparts is null");
                    return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_DATA_FOUND, false, null);
                }

            } else {
                // profile data not found
                return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_PROFILE_FOUND, false, null);

            }

        } catch (Exception e) {
            e.printStackTrace();
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_PROFILE_FOUND, false, null);
        }

        return Response.ok("failed").build();
    }
}