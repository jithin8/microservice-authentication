package com.jitihn.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.json.Json;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;
import com.jitihn.dao.AccountRepository;
import com.jitihn.dao.ProfileRepository;
import com.jitihn.dao.RoleRepository;
import com.jitihn.dto.AccountDTO;
import com.jitihn.jwt.TokenUtils;
import com.jitihn.kafka.KafkaController;
import com.jitihn.model.Account;
import com.jitihn.model.JwtToken;
import com.jitihn.model.Profile;
import com.jitihn.model.Role;
import com.jitihn.parser.AccountParser;
import com.jitihn.parser.ProfileParser;
import com.jitihn.utils.Constant;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/account")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    Logger logger = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    JsonWebToken jwt;
    @Inject
    AccountRepository accountRepository;
    @Inject
    ProfileRepository profileRepository;

    @Inject
    KafkaController kafkaController;

    @Inject
    Validator validator;

    @Inject
    RoleRepository roleRepository;

    @GET
    public Response get() {
        return Response.ok("Hai").build();
    }

    @GET
    @Path("/update/{pid}")
    @RolesAllowed({ "ADMIN" })
    @Transactional
    public Response updateEmployees(@PathParam("pid") String pid) {
        Account entity = accountRepository.findByPIdForUpdate(pid);
        if (entity != null) {
            entity.setActive(!entity.getActive());
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", new AccountParser(entity));
            return Utils.JsonResponse(Status.OK, Constant.Message.DATA_UPDATED, true, obj);
        }
        return Utils.JsonResponse(Status.EXPECTATION_FAILED, Constant.Message.SOMETHING_WENT_WRONG, false, null);
    }

    @GET
    @Path("/listemployees")
    @RolesAllowed({ "ADMIN" })
    public Response listEmployees() {
        List<AccountParser> accounts = accountRepository.findEmployees();
        if (accounts != null && accounts.size() > 0) {
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", accounts);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.LIST_SUCCESS, true, obj);
        }
        return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.NO_DATA_FOUND, false, null);
    }

    @POST
    @Path("/createadmin")
    @Transactional
    @PermitAll
    public Response createAdmin(AccountDTO accountDTO) {
        logger.info("Create Admin Account called " + accountDTO);
        // Set<ConstraintViolation<AccountDTO>> violations =
        // validator.validate(accountDTO);
        // if (!violations.isEmpty()) {
        // String message = Utils.validationResult(violations);
        // return Utils.JsonResponse(Status.NOT_ACCEPTABLE, message, false, null);
        // }
        Account accountExist = accountRepository.findByUsername("ADMIN");
        if (accountExist != null) {
            return Utils.JsonResponse(Status.CONFLICT, Constant.Message.USER_ALREADY_EXIST, false, null);
        } else {
            Role role = roleRepository.findByRole(accountDTO.getUsertype());
            if (role != null) {
                accountDTO.setRole(role);
                accountDTO.setPid(Utils.generateUniqueId());
                Account account = new Account(accountDTO);
                account.setActive(true); // creating status
                account.persist();
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("data", new AccountParser(account));
                logger.info(Constant.Message.USER_CREATED_SUCCESSFULLY);
                return Utils.JsonResponse(Status.CREATED, Constant.Message.USER_CREATED_SUCCESSFULLY, true, obj);
            } else {
                return Utils.JsonResponse(Status.CONFLICT, Constant.Message.ROLE_DOESNOT_EXIST, false, null);
            }
        }
    }

    @POST
    @Path("/create")
    @Transactional
    @RolesAllowed({ Constant.Usertype.ADMIN })
    public Response createAccount(AccountDTO accountDTO) {
        logger.info("Create Account called " + accountDTO);
        Set<ConstraintViolation<AccountDTO>> violations = validator.validate(accountDTO);
        if (!violations.isEmpty()) {
            String message = Utils.validationResult(violations);
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, message, false, null);
        }
        Account accountExist = accountRepository.findByUsername(accountDTO.getUsername());
        if (accountExist != null) {
            return Utils.JsonResponse(Status.CONFLICT, Constant.Message.USER_ALREADY_EXIST, false, null);
        } else {
            Role role = roleRepository.findByRole(accountDTO.getUsertype());
            if (role != null) {
                accountDTO.setRole(role);
                accountDTO.setPid(Utils.generateUniqueId());
                Account account = new Account(accountDTO);
                account.setActive(true); // creating status
                account.persist();
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("data", new AccountParser(account));

                // kafka send topic
                Gson gson = new Gson();
                String jsonString = gson.toJson(account);
                String kafkaMessage = Utils.objectToJson(jsonString);
                kafkaController.produce(kafkaMessage);

                logger.info(Constant.Message.USER_CREATED_SUCCESSFULLY);
                return Utils.JsonResponse(Status.CREATED, Constant.Message.USER_CREATED_SUCCESSFULLY, true, obj);
            } else {
                return Utils.JsonResponse(Status.CONFLICT, Constant.Message.ROLE_DOESNOT_EXIST, false, null);
            }
        }
    }

    @POST
    @Path("/login")
    @PermitAll
    public Response login(AccountDTO accountDTO) {
        logger.info("user login called " + accountDTO);
        Account account = accountRepository.userLogin(accountDTO);
        if (account != null) {
            String token = "";
            try {
                accountDTO.setPid(account.getPid());
                accountDTO.setRole(account.getRole());
                JwtToken jwtToken = new JwtToken(account);
                List<String> accessGroup = new ArrayList<>();
                accessGroup.add(accountDTO.getRole().getRolename());
                jwtToken.setGroups(accessGroup);
                token = TokenUtils.generateTokenString(jwtToken);
                accountDTO.setToken(token);
            } catch (Exception e) {
                e.printStackTrace();
                return Utils.JsonResponse(Status.INTERNAL_SERVER_ERROR, Constant.Message.SOMETHING_WENT_WRONG, false,
                        null);
            }
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", new AccountParser(accountDTO));
            return Utils.JsonResponse(Status.OK, Constant.Message.TOKEN_GENERATED, true, obj);
        } else {
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.LOGIN_FAILED, false, null);
        }
    }

    @POST
    @Path("/changepassword")
    @PermitAll
    @Transactional
    public Response changePassword(AccountDTO accountDTO) {
        // check for field set
        String errorCheck = accountRepository.changePassCheck(accountDTO);
        if (errorCheck == null) {
            Account account = accountRepository.findByAccountForUpdate(accountDTO);
            if (account != null) {
                account.setPassword(accountDTO.getNewpassword());
                return Utils.JsonResponse(Status.OK, Constant.Message.PASSWORD_CHANGED, true, null);
            } else {
                return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_DATA_FOUND, false, null);
            }
        } else {
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, errorCheck, false, null);
        }
    }

    // @GET
    // @Path("/sendmail/{msg}")
    // public Response sendMail(@PathParam("msg") String msg) {
    // kafkaController.produce(msg);
    // return Response.ok(msg).build();
    // }

    @GET
    @Path("/profile/{account_pid}")
    public Response getProfile(@PathParam("account_pid") String pid) {
        Account account = accountRepository.findByPid(pid);
        if (account != null) {
            Profile p = profileRepository.findByAccountId(account.getId());
            if (p != null) {
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("data", new ProfileParser(p));
                return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_FOUND, true, obj);
            } else {
                // profile not found
                return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_PROFILE_FOUND, false, null);
            }
        } else {
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            if (exception instanceof NotAuthorizedException) {
                code = ((NotAuthorizedException) exception).getResponse().getStatus();
            }
            return Response.status(code).entity(Json.createObjectBuilder().add("status", false)
                    .add("error", exception.getLocalizedMessage()).add("code", code).build()).build();
        }

    }

}
