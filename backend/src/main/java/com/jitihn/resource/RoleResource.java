package com.jitihn.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.json.Json;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.jitihn.dao.RoleRepository;
import com.jitihn.dto.RoleDTO;
import com.jitihn.model.Role;
import com.jitihn.utils.Constant;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/role")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource {

    Logger logger = LoggerFactory.getLogger(RoleResource.class);

    @Inject
    JsonWebToken jwt;

    @Inject
    Validator validator;

    @Inject
    RoleRepository roleRepository;

    @POST
    @Path("/add")
    @Transactional
    public Response add(RoleDTO roleDTO) {
        logger.info("Create Role called " + roleDTO);
        roleDTO.setPid(Utils.generateUniqueId());
        Role role = new Role(roleDTO);
        logger.info("Create Role called " + roleDTO);
        Set<ConstraintViolation<RoleDTO>> violations = validator.validate(roleDTO);
        if (!violations.isEmpty()) {
            String message = Utils.validationResult(violations);
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, message, false, null);
        }

        if (roleRepository.ifRoleExist(role)) {
            logger.info(Constant.Message.ALREADY_EXIST);
            return Utils.JsonResponse(Status.CONFLICT, Constant.Message.ALREADY_EXIST, false, null);
        }

        if (roleRepository.findByRole(role.getRolename()) != null) {
            logger.info(Constant.Message.ALREADY_EXIST);
            return Utils.JsonResponse(Status.CONFLICT, Constant.Message.ALREADY_EXIST, false, null);
        }

        role.persist();
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("data", role);
        logger.info(Constant.Message.CREATED_SUCCESSFULLY);
        return Utils.JsonResponse(Status.CREATED, Constant.Message.CREATED_SUCCESSFULLY, true, obj);
    }

    @POST
    @Path("/update")
    @Transactional
    public Response update(RoleDTO roleDTO) {
        Role role = new Role(roleDTO);
        logger.info("Update Role called " + roleDTO);
        Set<ConstraintViolation<RoleDTO>> violations = validator.validate(roleDTO);
        if (!violations.isEmpty()) {
            String message = Utils.validationResult(violations);
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, message, false, null);
        }

        if (roleRepository.ifRoleExist(role)) {
            if (roleRepository.findByRole(role.getRolename()) != null) {
                logger.info(Constant.Message.ALREADY_EXIST);
                return Utils.JsonResponse(Status.CONFLICT, Constant.Message.ALREADY_EXIST, false, null);
            }
            role = roleRepository.findByPIdForUpdate(roleDTO.getPid());
            role.setRolename(roleDTO.getRolename());
            role.setDescription(roleDTO.getDescription());
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", role);
            logger.info(Constant.Message.DATA_UPDATED);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_UPDATED, true, obj);
        } else {
            logger.info(Constant.Message.NO_DATA_FOUND);
            return Utils.JsonResponse(Status.NOT_ACCEPTABLE, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @GET
    public Response findAll() {
        logger.info("finadall called");
        List<Role> roles = roleRepository.findRole();

        // return Response.ok(roles).build();
        if (roles != null && roles.size() > 0) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", roles);
            logger.info(Constant.Message.LIST_SUCCESS);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.LIST_SUCCESS, true, obj);
        } else {
            logger.info(Constant.Message.NO_DATA_FOUND);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @GET
    @Path("/{pid}")
    public Response getRole(@PathParam("pid") String pid) {
        Role role = roleRepository.findByPid(pid);
        if (role != null) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", new RoleDTO(role));
            logger.info(Constant.Message.DATA_FOUND);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_FOUND, true, obj);
        } else {
            logger.info(Constant.Message.NO_DATA_FOUND);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @GET
    @Path("/delete/{pid}")
    @Transactional
    public Response delete(@PathParam("pid") String pid) {
        logger.info("delete role " + pid);
        Role role = roleRepository.findByPid(pid);
        if (role != null) {
            // success
            roleRepository.deleteRole(pid);
            logger.info(Constant.Message.DATA_DELETED);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.DATA_DELETED, false, null);
        } else {
            logger.info(Constant.Message.NO_DATA_FOUND);
            return Utils.JsonResponse(Status.ACCEPTED, Constant.Message.NO_DATA_FOUND, false, null);
        }
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            if (exception instanceof NotAuthorizedException) {
                code = ((NotAuthorizedException) exception).getResponse().getStatus();
            }
            return Response.status(code).entity(Json.createObjectBuilder().add("status", false)
                    .add("error", exception.getLocalizedMessage()).add("code", code).build()).build();
        }

    }
}