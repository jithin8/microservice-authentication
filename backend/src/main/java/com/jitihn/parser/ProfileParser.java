package com.jitihn.parser;

import com.jitihn.dto.ProfileDTO;
import com.jitihn.model.Profile;

public class ProfileParser {
    private String username;
    private String email;
    private String pid;
    private String role_name;
    private String role_desc;
    private String mobile;
    private String profile_image;
    private Boolean active;

    public ProfileParser(ProfileDTO profileDTO) {
        this.username = profileDTO.getAccount().getUsername();
        this.email = profileDTO.getAccount().getEmail();
        this.pid = profileDTO.getPid();
        this.role_name = profileDTO.getAccount().getRole().getRolename();
        this.role_desc = profileDTO.getAccount().getRole().getDescription();
        this.active = profileDTO.getAccount().getActive();
        this.mobile = profileDTO.getMobile();
        this.profile_image = profileDTO.getProfile_image();
    }

    public ProfileParser(Profile profile) {
        this.username = profile.getAccount().getUsername();
        this.email = profile.getAccount().getEmail();
        this.pid = profile.getPid();
        this.role_name = profile.getAccount().getRole().getRolename();
        this.role_desc = profile.getAccount().getRole().getDescription();
        this.active = profile.getAccount().getActive();
        this.mobile = profile.getMobile();
        this.profile_image = profile.getProfile_image();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_desc() {
        return role_desc;
    }

    public void setRole_desc(String role_desc) {
        this.role_desc = role_desc;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}