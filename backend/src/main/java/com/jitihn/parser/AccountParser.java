package com.jitihn.parser;

import com.jitihn.dto.AccountDTO;
import com.jitihn.model.Account;

public class AccountParser {
    private String username;
    private String email;
    private String pid;
    private String role_name;
    private String role_desc;
    private String token;
    private Boolean active;

    public AccountParser(AccountDTO accountDTO) {
        this.username = accountDTO.getUsername();
        this.email = accountDTO.getEmail();
        this.pid = accountDTO.getPid();
        this.role_name = accountDTO.getRole().getRolename();
        this.role_desc = accountDTO.getRole().getDescription();
        this.token = accountDTO.getToken();
        this.active = accountDTO.getActive();
    }

    public AccountParser(Account account) {
        this.username = account.getUsername();
        this.email = account.getEmail();
        this.pid = account.getPid();
        this.role_name = account.getRole().getRolename();
        this.role_desc = account.getRole().getDescription();
        this.active = account.getActive();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_desc() {
        return role_desc;
    }

    public void setRole_desc(String role_desc) {
        this.role_desc = role_desc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}