package com.jitihn.dto;

import com.jitihn.model.Account;

public class ProfileDTO {
    Account account;
    String account_pid;
    String pid;
    String mobile;
    String profile_image;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getAccount_pid() {
        return account_pid;
    }

    public void setAccount_pid(String account_pid) {
        this.account_pid = account_pid;
    }

    @Override
    public String toString() {
        return "ProfileDTO [account=" + account + ", account_pid=" + account_pid + ", mobile=" + mobile + ", pid=" + pid
                + ", profile_image=" + profile_image + "]";
    }

}