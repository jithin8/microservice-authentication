package com.jitihn.dto;

import javax.validation.constraints.NotBlank;
import com.jitihn.model.Role;

import net.minidev.json.annotate.JsonIgnore;

public class AccountDTO {

    @NotBlank(message = "Username may not be blank")
    private String username;
    @NotBlank(message = "Email may not be blank")
    private String email;
    @NotBlank(message = "Password may not be blank")

    @JsonIgnore
    private String password;
    @JsonIgnore
    private String newpassword;
    @NotBlank(message = "Usertype may not be blank")
    private String usertype;
    private String pid;
    private String account_pid;
    private Role role;
    private String token;
    private Boolean active;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    @Override
    public String toString() {
        return "AccountDTO [active=" + active + ", email=" + email + ", password=" + password + ", pid=" + pid
                + ", role=" + role + ", token=" + token + ", username=" + username + ", usertype=" + usertype + "]";
    }

    public String getAccount_pid() {
        return account_pid;
    }

    public void setAccount_pid(String account_pid) {
        this.account_pid = account_pid;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}