package com.jitihn.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.jitihn.model.Account;
import com.jitihn.model.Role;

public class RoleDTO {

    public RoleDTO(Role role) {
        this.rolename = role.getRolename();
        this.description = role.getDescription();
        this.pid = role.getPid();
    }

    public RoleDTO(String rolename, String description, String pid) {
        this.rolename = rolename;
        this.description = description;
        this.pid = pid;
    }

    @NotBlank(message = "Role may not be blank")
    private String rolename;
    private String description;
    private String pid;
    private List<Account> accounts;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public RoleDTO() {
    }

    @Override
    public String toString() {
        return "RoleDTO [accounts=" + accounts + ", description=" + description + ", pid=" + pid + ", rolename="
                + rolename + "]";
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

}