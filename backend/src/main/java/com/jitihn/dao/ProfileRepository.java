package com.jitihn.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.jitihn.model.Profile;

import io.quarkus.hibernate.orm.panache.Panache;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ProfileRepository implements PanacheRepository<Profile> {

    public Profile findByPid(String pid) {
        return Profile.find("pid", pid).firstResult();
    }

    public Profile findByAccountId(Long id) {
        return Profile.find("account_id", id).firstResult();
    }

    public Profile findByPIdForUpdate(String pid) {
        EntityManager entityManager = Panache.getEntityManager();
        Profile profile = findByPid(pid);
        entityManager.lock(profile, LockModeType.PESSIMISTIC_WRITE);
        return profile;
    }
}