package com.jitihn.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.jitihn.model.Role;

import io.quarkus.hibernate.orm.panache.Panache;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class RoleRepository implements PanacheRepositoryBase<Role, Long> {

    public Role findByRole(String rolename) {
        return Role.find("rolename", rolename).firstResult();
    }

    public Role findByPid(String pid) {
        return Role.find("pid", pid).firstResult();
    }

    // true if role exist
    public boolean ifRoleExist(Role role) {
        return findByPid(role.getPid()) != null ? true : false;
    }

    public Role findByPIdForUpdate(String pid) {
        EntityManager entityManager = Panache.getEntityManager();

        Role role = findByPid(pid);
        // lock with the PESSIMISTIC_WRITE mode type : this will generate a SELECT ...
        // FOR UPDATE query
        entityManager.lock(role, LockModeType.PESSIMISTIC_WRITE);
        return role;
    }

    public void deleteRole(String pid) {
        Role.delete("pid", pid);
    }

    public List<Role> findRole() {
        return Role.list("order by id");
    }
}