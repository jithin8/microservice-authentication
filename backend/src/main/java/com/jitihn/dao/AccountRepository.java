package com.jitihn.dao;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import com.jitihn.dto.AccountDTO;
import com.jitihn.model.Account;
import com.jitihn.parser.AccountParser;
import com.jitihn.utils.Constant;
import io.quarkus.hibernate.orm.panache.Panache;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class AccountRepository implements PanacheRepositoryBase<Account, Long> {

    public Account findByPid(String pid) {
        return Account.find("pid", pid).firstResult();
    }

    public Account findByUsername(String username) {
        Account a = Account.find("username", username).firstResult();
        return a;
    }

    public Account findByPIdForUpdate(String pid) {
        EntityManager entityManager = Panache.getEntityManager();
        Account account = findByPid(pid);
        entityManager.lock(account, LockModeType.PESSIMISTIC_WRITE);
        return account;
    }

    public Account userLogin(AccountDTO accountDTO) {
        return Account.find("username = ?1 and password = ?2 and active = ?3", accountDTO.getUsername(),
                accountDTO.getPassword(), true).firstResult();
    }

    public Account userFind(AccountDTO accountDTO) {
        return Account.find("pid = ?1 and password = ?2", accountDTO.getAccount_pid(), accountDTO.getPassword())
                .firstResult();
    }

    public Account findByAccountForUpdate(AccountDTO accountDTO) {
        EntityManager entityManager = Panache.getEntityManager();
        Account account = userFind(accountDTO);
        if (account != null) {
            entityManager.lock(account, LockModeType.PESSIMISTIC_WRITE);
            return account;
        }
        return null;
    }

    public List<AccountParser> findEmployees() {
        // using stream
        Stream<Account> accounts = Account.streamAll();
        List<AccountParser> accountEmp = accounts.map(p -> new AccountParser(p))
                .filter(n -> n.getRole_name().equalsIgnoreCase(Constant.Usertype.EXEC))
                .sorted(Comparator.comparing(AccountParser::getUsername)).collect(Collectors.toList());
        return accountEmp;
    }

    public String changePassCheck(AccountDTO accountDTO) {
        String accountPid = accountDTO.getAccount_pid();
        String password = accountDTO.getPassword();
        String newpassword = accountDTO.getNewpassword();
        String msg = null;

        if (accountPid == null || accountPid.length() == 0) {
            msg = "Account pid not set";
        } else if (password == null || password.length() == 0) {
            msg = "Current password not set";
        } else if (newpassword == null || newpassword.length() == 0) {
            msg = "New password not set";
        } else if (password.equalsIgnoreCase(newpassword)) {
            msg = "Password are same";
        }
        return msg;
    }
}