package com.jitihn.utils;

public class Constant {
    public class Message {
        public static final String USER_ALREADY_EXIST = "User already Exist";
        public static final String ALREADY_EXIST = "Already Exist";
        public static final String ROLE_DOESNOT_EXIST = "Role not exist";
        public static final String USER_NOT_EXIST = "User not exist";
        public static final String USER_CREATED_SUCCESSFULLY = "Account created successfully";
        public static final String CREATED_SUCCESSFULLY = "Created successfully";
        public static final String LOGIN_SUCCESS = "Login success";
        public static final String LOGIN_FAILED = "Login failed";
        public static final String LIST_SUCCESS = "List success";
        public static final String DATA_UPDATED = "Data updated";
        public static final String NO_DATA_FOUND = "No data found";
        public static final String NO_PROFILE_FOUND = "No profile found";
        public static final String SOMETHING_WENT_WRONG = "Something went wrong";
        public static final String TOKEN_GENERATED = "Token generated";
        public static final String DATA_FOUND = "Data found";
        public static final String DATA_DELETED = "Data deleted";
        public static final String ACCOUNT_PID_NOTSET = "Account pid not set";
        public static final String PASSWORD_CHANGED = "Password changed";
    }

    public class Usertype {
        public static final String ADMIN = "ADMIN";
        public static final String EXEC = "EXEC";
    }

    // token expire in one day
    public static final long TOKEN_EXPIRED = 24 * 60 * 60;
    // public static final long TOKEN_EXPIRED = 60;
}