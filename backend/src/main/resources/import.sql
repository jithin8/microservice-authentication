INSERT INTO role (id, description, rolename, pid) VALUES
    (1, 'Admin user', 'ADMIN', '8326e90c-9c09-456b-bf1c-1985ba669746'),
    (2, 'Executive user', 'EXEC', '2c232869-6229-4da5-90a8-24c929c29ac6');
    
INSERT INTO account(id, active, email, password, pid, username, role_id) VALUES 
    (1, true, 'admin@gmail.com', 'admin', 'admin29116-026f-4c72-8e9d-eaa0831b9331', 'admin', 1),
    (2, true, 'jithin@gmail.com', 'jithin', 'user129116-026f-4c72-8e9d-eaa0831b9331', 'jithin', 2);